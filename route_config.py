import views


class RouteConfig:
    def __init__(self, app):
        self.__app = app

    def init(self):
        self.__app.add_url_rule("/", "index", views.index)
        self.__app.add_url_rule("/index", "index", views.index)
        self.__app.add_url_rule("/add", "add", views.add, methods=["GET", "POST"])
