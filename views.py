from flask import render_template, request, redirect, url_for
from datetime import datetime
from models import User

__bookmarks = []


def __store_bookmark(url):
    __bookmarks.append(dict(
        url=url,
        user="farooqam",
        date=datetime.utcnow()
    ))


def index():
    return render_template("index.html", title="Title passed from view to template",
                           text="Text passed from view to template.",
                           user=User(first_name="Farooq", last_name="Mahmud"))


def add():
    if request.method == "POST":
        __store_bookmark(request.form["url"])
        return redirect(url_for("index"))

    return render_template("add.html")


def error(error):
    return render_template("{0}.html".format(error.code)), error.code
