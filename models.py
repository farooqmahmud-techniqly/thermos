from collections import namedtuple

User = namedtuple("User", "first_name last_name")