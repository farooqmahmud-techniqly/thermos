import views


class ErrorHandlerConfig:
    def __init__(self, app):
        self.__app = app

    def init(self):
        self.__app.register_error_handler(404, lambda error: views.error(error))
        self.__app.register_error_handler(500, lambda error: views.error(error))
