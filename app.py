from flask import Flask
from route_config import RouteConfig
from error_handler_config import ErrorHandlerConfig

app = Flask(__name__)

errorHandlerConfig = ErrorHandlerConfig(app)
errorHandlerConfig.init()

routeConfig = RouteConfig(app)
routeConfig.init()

if __name__ == "__main__":
    app.run("127.0.0.1", 7923, debug=False)
